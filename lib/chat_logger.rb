require_relative '../database/database_wrapper'

module ObserveChat
  # Logger
  # Logs users, channels, networks, messages and other info.
  # Message processing should be done here, not on the bot itself.
  class ChatLogger
    def initialize(settings)
      @db = DatabaseWrapper.new(settings)
    end

    def log(msg)
      @db.save_message(msg)
    end

    def find(params)
      @db.find(params)
    end
  end
end
