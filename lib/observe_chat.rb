require 'sinatra/base'

require_relative 'observe_chat/version'
require_relative 'settings'
require_relative 'observe_bot'

# Namespace for ObserveChat
module ObserveChat
  class Error < StandardError; end

  trap 'INT' do
    Thread.list.each do |t|
      t.exit unless t == Thread.current
      exit
    end
  end

  # ObserveChat
  # Application that handles bots.
  class ObserveChat < Sinatra::Base
    def self.bots
      @@bots
    end

    def load_setting(path = './observe_chat.yml')
      @options = Settings.parse(path)
    end

    def configure_bot
      server = @options.irc.first[0]
      channels = @options.irc.first[1]
      bot_name = @options.settings('bot_name')

      @@bots = ObserveBot.new(server, channels, bot_name, @options.database)
    end

    def start_observer
      bots_t = Thread.new { @@bots.start }
      ObserveChat.run!
      bots_t.join
    end

    # REST API

    get '/messages' do
      ObserveChat.bots.find(params)
    end

    get '/servers' do
      'GET servers'
    end

    get '/channels' do
      'GET channels'
    end

    get '/users' do
      'GET users'
    end
  end
end
