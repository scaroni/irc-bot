require 'yaml'

module ObserveChat
  # Settings
  # Factory for I/O settings from configuration file.
  class Settings
    YAML_ROOT = ['observe_chat'].freeze

    SETTINGS = (YAML_ROOT + ['settings']).freeze
    IRC = (YAML_ROOT + ['irc']).freeze
    DATABASE = (YAML_ROOT + ['database']).freeze

    # Options
    # What Settings spits out.
    class Options
      def find(path)
        @cfg_file.dig(*path)
      end

      def initialize(fpath)
        @cfg_file = YAML.load_file(fpath) || {} if File.exist?(fpath)
        raise ArgumentError, "Problem parsing #{fpath}" if @cfg_file.empty?

        @set = find(SETTINGS)
        @irc = {}
        find(IRC).each { |k, v| @irc[k] = v }
        @db = find(DATABASE)
      end

      def settings(key = nil)
        key.nil? ? @set.clone : @set[key]
      end

      def irc(key = nil)
        key.nil? ? @irc.clone : @irc[key]
      end

      def database(key = nil)
        key.nil? ? @db.clone : @db[key]
      end
    end
    private_constant :Options

    def self.parse(path)
      Options.new(path)
    end
  end
end
