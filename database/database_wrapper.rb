require 'yaml'
require 'json'
require 'mongo'

module ObserveChat
  KEYWORDS = %i[nick message time channel server].freeze

  # DatabaseWrapper
  # Wraps the Ruby MongoDB driver into a more intuitive class.
  class DatabaseWrapper
    def initialize(settings)
      client = Mongo::Client.new(["#{settings['host']}:#{settings['port']}"],
                                 database: settings['db'])
      @collection = client.database.collection(settings['collection'])
    end

    def save_message(msg)
      @collection.insert_one(
        nick: msg.user.nick,
        message: msg.message,
        time: msg.time,
        channel: msg.channel.to_s,
        server: msg.server
      )
    end

    def print_messages
      all.each { |row| puts row.inspect }
    end

    def all
      @collection.find
    end

    def find(params)
      @collection.find(DatabaseWrapper.filter(params)).find.to_a.to_json
    end

    def self.filter(hash)
      f = hash.select { |k, _| KEYWORDS.include? k.to_sym }
      f.each_with_object({}) { |(k, v), m| m[k.to_sym] = v }
    end
  end
end
